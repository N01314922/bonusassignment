﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="palindrome.aspx.cs" Inherits="BonusApplication.palindrome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server" ID="label1" Text="Enter String"></asp:Label>
            <asp:Textbox  runat="server" ID="paliinputtxt"></asp:Textbox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator" ControlToValidate="paliinputtxt" 
                Display="Static" ErrorMessage="Please Enter the text to check palindrome"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionnValidator" ControlToValidate="paliinputtxt"
                ErrorMessage="only strings are allowed" ValidationExpression="^[a-zA-Z\s]+$"></asp:RegularExpressionValidator>
            </div>
            <div>
                <asp:Button runat="server" ID="submit" Text="Check if palindrome" OnClick="CheckPalindrome"/>
                <asp:Button runat="server" ID="clear" Text="Clear Values" OnClick="ClearValues"/>
            </div>
            <div>
                <asp:Label runat="server" ID="output" Text=""></asp:Label>
            </div>
       
    </form>
</body>
</html>
