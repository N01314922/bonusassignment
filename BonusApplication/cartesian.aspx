﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cartesian.aspx.cs" Inherits="BonusApplication.cartesian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             
            <asp:Label runat="server" ID="xlabel" Text="Enter X value"></asp:Label>
            <asp:Textbox  runat="server" ID="xvalue"></asp:Textbox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator" ControlToValidate="xvalue" 
                Display="Static" ErrorMessage="Please Enter the valueof X"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator" ControlToValidate="xvalue"
                ErrorMessage="Only numbers are allowed" ValidationExpression="^[+-]?([0-9]*\.?[0-9]+[0-9]*)([eE][+-]?[0-9]+)?$"></asp:RegularExpressionValidator>
         </div>

             <div>
             
            <asp:Label runat="server" ID="ylabel" Text="Enter y value"></asp:Label>
            <asp:Textbox  runat="server" ID="yvalue"></asp:Textbox>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="yvalue" 
                Display="Static" ErrorMessage="Please Enter the valueof y"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ControlToValidate="yvalue"
                ErrorMessage="Only numbers are allowed" ValidationExpression="^[+-]?([0-9]*\.?[0-9]+[0-9]*)([eE][+-]?[0-9]+)?$"></asp:RegularExpressionValidator>
         </div>
        <div>
             <asp:Button runat="server" ID="submit" Text="submit values" OnClick="SubmitValues"/>
             <asp:Button runat="server" ID="clear" Text="Clear Values" OnClick="ClearValues"/>
        </div>
        <div>
                <asp:Label runat="server" ID="output" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
