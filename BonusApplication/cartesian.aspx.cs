﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusApplication
{
    public partial class cartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SubmitValues(object sender, EventArgs e)
        {
            int x, y;

            x = Convert.ToInt32(xvalue.Text);
            y = Convert.ToInt32(yvalue.Text);

            if(x > 0 &&  y > 0 )
            {
                output.Text = "(" + x + "," + y + ") Falls in quadrant 1 ";
            }
            if (x < 0 && y > 0)
            {
                output.Text = "(" + x + "," + y + ") Falls in quadrant 2 ";
            }
            if (x > 0 && y < 0)
            {
                output.Text = "(" + x + "," + y + ") Falls in quadrant 3 ";
            }
            if (x < 0 && y < 0)
            {
                output.Text = "(" + x + "," + y + ") Falls in quadrant 4 ";
            }
        }

        protected void ClearValues(object sender, EventArgs e)
        {
            xvalue.Text = "";
            yvalue.Text = "";
           output.Text = "";
        }

    }
}