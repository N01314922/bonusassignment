﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusApplication
{
    public partial class PrimeNumber : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CheckIfPrime(object sender, EventArgs e)
        {
            int count = 0;
            int x;
            x = Convert.ToInt32(textBox.Text);
              for (int i = 1; i <= (x / 2); i++)
              {
                  if (x % 2 == 0)
                  {
                      count++;
                  }
              }

              if (count == 1)
              {
                  output.Text = x + " is prime number";
              }

              else
              {
                  output.Text = x + " is not prime number";
              }

          }

            

        protected void ClearValues(object sender, EventArgs e)
        {
            textBox.Text = "";
            output.Text = "";
        }
    }
}